from flask import Flask
from . import index, newpage

def create_app():
    # create and configure the app
    app = Flask(__name__)
    app.register_blueprint(index.bp)
    app.register_blueprint(newpage.bp)
    return app
