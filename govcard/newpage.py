from flask import (
    Blueprint, g, redirect, render_template, request, url_for
)

bp = Blueprint('newpage', __name__)

@bp.route('/api/hello')
def hello():
    return {"message": "Hello"}, 200

@bp.route('/api/stuff')
def get_stuff():
    try:
        id = int(request.args.get("id", -1))
        page = int(request.args.get("page", ""))
    except:
        id = -1
    if id < 0:
        return {"message": "id not found"}, 404
    return {"id": id, "page": page}

@bp.route('/api/movies/<name>')
def get_other(name):
    return {"nome": name}

